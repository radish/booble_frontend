***************
Booble Frontend
***************
Aurelia-based frontend for Booble rooms booking system.

============
Installation
============

-----------------
Development setup
-----------------
The quick setup can be performed using `Docker Compose
<https://docs.docker.com/compose/>`_ (recommended):

.. code-block:: bash

    $ docker-compose -f Docker/dev/docker-compose.yml up

This command will install all the dependencies and start HTTP server listening
on 127.0.0.1:9000.

=======
Authors
=======
For complete list of authors check `contributors
<https://gitlab.com/radish/draughts-state-viewer/graphs/master/contributors>`_.
