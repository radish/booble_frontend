#!/usr/bin/env sh

USERID=$(stat -c "%u" /code)
GROUPID=$(stat -c "%g" /code)


if ! getent passwd $USERID > /dev/null 2>&1; then
  echo Creating user...
  adduser -D -u $USERID code
fi

USRNAME=$(getent passwd $USERID | cut -d: -f1)

cd /code

echo Installing npm packages...
gosu $USERID:$GROUPID npm --loglevel=error --progress=false install

echo Installing jspm packages...
gosu $USERID:$GROUPID jspm --log err install

echo HTTP server is now running
nginx -g "user $USRNAME;"
gosu $USRNAME tail -q -F /var/log/access.log ; kill nginx
