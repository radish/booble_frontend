import {autoinject} from 'aurelia-dependency-injection';
import {Router} from 'aurelia-router';

import AuthService from './services/auth';

@autoinject
export class Home {
  auth: AuthService;
  router: Router;

  constructor(auth: AuthService, router: Router) {
    this.auth = auth;
    this.router = router;
  }

  async logout() {
    await this.auth.logout();
    this.router.navigate('/users/login');
  }
}
