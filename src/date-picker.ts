import {inject} from "aurelia-dependency-injection";
import {customAttribute, bindable} from 'aurelia-framework';

@inject(Element)
export class DatePickerCustomAttribute {
    element: Element;
    @bindable otherMinDate: string = "";
    @bindable format: string = "DD.MM.YYYY HH:mm"

    constructor(element: Element) {
        this.element = element;
    }

    createEvent(name: any) {  
        let event = document.createEvent('Event');
        event.initEvent(name, true, true);
        return event;
    }

    fireEvent(element: any, name: string) {  
        let event = this.createEvent(name);
        element.dispatchEvent(event);
    }

    attached() {
        $(this.element).datetimepicker({
            format: this.format,
            minDate: new Date,
            stepping: 5

        }).on('dp.change', (e : any) => {
            if (this.otherMinDate != "") {
                $("#" + this.otherMinDate).data("DateTimePicker").minDate(e.date);
            }

            this.fireEvent(e.target, 'input');
        })
    }

    detached() {
        $(this.element).off('dp.change');
    }
}
