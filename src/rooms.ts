import {HttpClient} from 'aurelia-http-client';
import {autoinject} from 'aurelia-dependency-injection';
import {Router} from 'aurelia-router';
import AuthService from './services/auth';
import RoomsService from './services/rooms';

@autoinject
export class Rooms {
  private auth: AuthService;
  private router: Router;
  private http: HttpClient;
  private roomsService: RoomsService;


  rooms: Array<any>;
  description: string;
  assets: Array<Object>;
  searchVisible: boolean = null;
  currentRoomId: number;
  wrapToggleContent: string = "Advanced search ↓";

  iSearchText: string = "";
  advName: string = "";
  advMin: number;
  advMax: number;
  advAddress: string = "";
  advDescription: string = "";
  advSearchObj: any;

  constructor(auth: AuthService, router: Router, http: HttpClient,
              roomsService: RoomsService) {
    this.auth = auth;
    this.router = router;
    this.http = http;
    this.roomsService = roomsService;
  }

  async activate(params: any) {
    this.rooms = await this.roomsService.get_rooms();

    this.currentRoomId = Number(params.roomId);
    if (!isNaN(this.currentRoomId)) {
      let room = this.rooms.find((room) => room.id === this.currentRoomId);

      this.description = room.description;
      this.assets = await this.get_assets();
    }
  }

  private async get_assets() {
    let response = await this.http.get(
      'assets/room/' + this.currentRoomId + '/'
    );
    return response.content;
  }

  private advancedSearch() {
    this.advSearchObj = {
        "name": this.advName,
        "min": this.advMin,
        "max": this.advMax,
        "address": this.advAddress,
        "description": this.advDescription
    }
  }

  private wrapForm(){
    if(this.searchVisible === null){
      this.searchVisible = true;
      this.wrapToggleContent = "Simple search ↑";
    }
    else{
      this.searchVisible = null;
      this.wrapToggleContent = "Advanced search ↓";
    }
  }
}
