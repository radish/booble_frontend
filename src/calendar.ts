import {autoinject} from "aurelia-dependency-injection";
import {Router} from "aurelia-router";

import AuthService from "./services/auth";
import RoomsService from './services/rooms';


@autoinject
export class Calendar {
  rooms: Array<any>;
  currentRoom: any;

  firstday: Date;
  firstdayString: String = "";
  lastday: Date;
  lastdayString: String = "";

  isCyclic: boolean = false;
  daysOfTheWeek: Array<string> = [
    'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'
  ];

  reservations: any = {};
  mouseIsDown: boolean = false;

  constructor(private auth: AuthService, private router: Router,
              private roomsService: RoomsService) {
    var curr = new Date;
    this.firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()+1));
    this.lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+7));
    this.firstdayString = this.firstday.toLocaleDateString();
    this.lastdayString = this.lastday.toLocaleDateString();

    for (let i = 0; i < 96; i++) {
      this.reservations[i] = {};
    }
  }

  async activate(params: any) {
    this.rooms = await this.roomsService.get_rooms();

    let roomId = Number(params.roomId);
    if (!isNaN(roomId)) {
      this.currentRoom = await this.roomsService.get_room(roomId);
    }
  }

  changeDate(direction: string) {
    if(direction == 'left')
    {
      this.firstday = new Date(this.firstday.getFullYear(),
                               this.firstday.getMonth(),
                               this.firstday.getDate() - 7);
      this.lastday = new Date(this.lastday.getFullYear(),
                              this.lastday.getMonth(),
                              this.lastday.getDate() - 7);
    }
    else if (direction == 'right')
    {
      this.firstday = new Date(this.firstday.getFullYear(),
                               this.firstday.getMonth(),
                               this.firstday.getDate() + 7);
      this.lastday = new Date(this.lastday.getFullYear(),
                              this.lastday.getMonth(),
                              this.lastday.getDate() + 7);
    }
    this.firstdayString = this.firstday.toLocaleDateString();
    this.lastdayString = this.lastday.toLocaleDateString();
  }

  createReservation(event: MouseEvent, colNumber: number, bigRowNumber: number,
                    smallRowNumber: number) {
    if (event.button === 2) {
      return;
    }
    console.debug('Allowing event listening');
    this.mouseIsDown = true;
    window.addEventListener('mouseup', () => {
      console.debug('Blocking event listening');
      this.mouseIsDown = false;
    });

    this.reservations[colNumber][4 * bigRowNumber + smallRowNumber] = true;
  }
}
