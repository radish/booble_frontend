import {autoinject} from 'aurelia-dependency-injection';
import {Router} from 'aurelia-router';
import AuthService from '../services/auth';

@autoinject
export class LoginForm {
  email: string;
  password: string;
  auth: AuthService;
  router: Router;

  email_error: string;
  password_error: string;
  other_error: string;

  constructor(auth: AuthService, router: Router) {
    this.auth = auth;
    this.router = router;
  }

  async login() {
    if (this.email && this.password) {
      try {
        await this.auth.login(this.email, this.password);
        this.email_error = null;
        this.password_error = null;
        this.other_error = null;

        this.router.navigate('/');
      }
      catch (error) {
        this.email_error = error.content.email;
        this.password_error = error.content.password;
        this.other_error = error.content.non_field_errors;
      }
    }
    else {
      if (!this.email) {
        this.email_error = 'Email field cannot be empty.'
      }
      else {
        this.email_error = null;
      }
      if (!this.password) {
        this.password_error = 'Password field cannot be empty.'
      }
      else {
        this.password_error = null;
      }
    }
  }
}
