import {Calendar} from '../calendar';
import {bindable, bindingMode} from 'aurelia-framework';
import {inject} from 'aurelia-dependency-injection';

@inject(Calendar, Element)
export class Reservation {
  @bindable({ defaultBindingMode: bindingMode.twoWay }) reservations;
  @bindable colNumber: number;
  @bindable bigRowNumber: number;
  @bindable smallRowNumber: number;

  height: number;
  width: number;
  top: number = 0;
  bottom: number = 0;
  baseBoundingRect: any;

  handleStretchingMouseMove: EventListener;
  cancelStretchingMouseMoveHandling: EventListener;
  handleDraggingMouseMove: EventListener;
  cancelDraggingMouseMoveHandling: EventListener;

  constructor(private calendar: Calendar, private element: HTMLElement) {
    this.handleStretchingMouseMove = (e: MouseEvent) => {
      let elemBoundingRect = this.element.getBoundingClientRect();

      let numberOfQuarters = Math.max(
        0, Math.ceil(
          (e.clientY - elemBoundingRect.top - this.height) / this.height
        )
      );

      let height = numberOfQuarters * this.height + this.top - 1;
      this.element.style.height = `${height}px`;
    };

    this.cancelStretchingMouseMoveHandling = () => {
      window.removeEventListener('mousemove', this.handleStretchingMouseMove);
    }

    this.handleDraggingMouseMove = (e: MouseEvent) => {
      let yPos = Math.ceil(
        (e.clientY - this.baseBoundingRect.top - this.height) / this.height
      );
      let xPos = Math.ceil(
        (e.clientX - this.baseBoundingRect.left - this.width) / this.width
      );

      let rowNumber = yPos +  4 * Number(this.bigRowNumber) + Number(this.smallRowNumber);
      let colNumber = xPos + Number(this.colNumber);

      // Horizontal movement
      if (0 <= colNumber && colNumber <= 6) {
        this.element.style.left = `${xPos * this.width}px`;
      }

      // Vertical movement
      if (0 <= rowNumber && rowNumber <= 96) {
        let newTop = yPos * this.height;
        this.top = newTop;

        this.element.style.top = `${this.top}px`;
      }
    }

    this.cancelDraggingMouseMoveHandling = (e: MouseEvent) => {
      window.removeEventListener('mousemove', this.handleDraggingMouseMove);
    }
  }

  attached() {
    this.height = this.element.clientHeight + 1;
    this.width = this.element.clientWidth + 1;
    this.baseBoundingRect = this.element.getBoundingClientRect();

    // This seems like a race condition, but it appears to work without if below
    // TODO: check in browsers other than Chromium
    if (this.calendar.mouseIsDown) {
      console.debug('Starting event listening');
      window.addEventListener('mouseup',
                              this.cancelStretchingMouseMoveHandling);
      window.addEventListener('mousemove', this.handleStretchingMouseMove);
    }
    else {
      console.warn('Event listening was blocked - REPORT THIS!!! //Dominik');
    }
  }

  handle_mousedown() {
    window.addEventListener('mouseup', this.cancelDraggingMouseMoveHandling);
    window.addEventListener('mousemove', this.handleDraggingMouseMove);
  }

  handle_mousedown_on_stretch_bar() {
    window.addEventListener('mouseup', this.cancelStretchingMouseMoveHandling);
    window.addEventListener('mousemove', this.handleStretchingMouseMove);
  }
}
