import {HttpClient} from 'aurelia-http-client';
import {autoinject} from 'aurelia-dependency-injection';
import appConfig from '../app-config';

@autoinject
export class RegistrationForm {
  urls: any = {
    register: appConfig.auth_url + 'register',
  };

  first_name: string;
  last_name: string;
  email: string;
  password: string;
  http: HttpClient;

  first_name_error: string;
  last_name_error: string;
  email_error: string;
  password_error: string;
  registration_successful: boolean;

  constructor(http: HttpClient) {
    this.http = http;
  }

  async register() {
    try {
      await this.http.post(
        this.urls.register,
        {
          email: this.email,
          password: this.password,
          first_name: this.first_name,
          last_name: this.last_name
        }
      );

      this.first_name_error = null;
      this.last_name_error = null;
      this.email_error = null;
      this.password_error = null;
      this.registration_successful = true;
    }
    catch (error) {
      this.first_name_error = error.content.first_name;
      this.last_name_error = error.content.last_name;
      this.email_error = error.content.email;
      this.password_error = error.content.password;
      this.registration_successful = false;
    }
  }
}
