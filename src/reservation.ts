import {autoinject} from "aurelia-dependency-injection";
import {Router} from "aurelia-router";
import {HttpClient} from 'aurelia-http-client';
import AuthService from "./services/auth";
import RoomsService from './services/rooms';


@autoinject
export class Reservation {
  private auth: AuthService;
  private router: Router;
  private http: HttpClient;
  private rooms: RoomsService

  reservations: Array<any>;
  cyclicReservations: Array<any> = [];

  constructor(auth: AuthService, router: Router, http: HttpClient, rooms: RoomsService) {
    this.auth = auth;
    this.router = router;
    this.http = http;
    this.rooms = rooms;
    this.reservations = [];

  }

  async activate(params: any) {
      this.getMyReservations();
  }

  async getMyReservations() {
    let response = await this.http.get(
      'rooms/reservations/'
    );
    for (let item of response.content) {
        if (item.datetime_ranges.length == 1) {
            this.populateReservations(item);
        } else if (item.datetime_ranges.length > 1) {
            this.populateCyclicReservations(item);
        } else {
            console.log("Error: unknown type of reservations/bad JSON response");
        }
    }
  }

  private async populateReservations(item: any) {
      let from_time = new Date(item.datetime_ranges[0][0]);
      let to_time = new Date(item.datetime_ranges[0][1]);
      let room = await this.rooms.get_room(item.id);
      let name = room.name;
      let reservation = {
          id: item.id,
          name: name,
          start_datetime: this.formatDate(from_time) + " " + this.formatTime(from_time),
          end_datetime: this.formatDate(to_time) + " " + this.formatTime(to_time),
      };
      this.reservations.push(reservation);
  }
  private async populateCyclicReservations(item: any) {
      // first date
      let from_time = new Date(item.datetime_ranges[0][0]);

      // next date in time range needed to calculate period
      let next_time = new Date(item.datetime_ranges[1][0]);

      // last date
      let last_index = item.datetime_ranges.length - 1;
      let to_time = new Date(item.datetime_ranges[last_index][1]);

      let difference: number = next_time - from_time;
      let difference_data = new Date(difference);
      let period = difference_data.getDate() - 1;

      let room = await this.rooms.get_room(item.id);
      let name = room.name;

      let reservation = {
          id: item.id,
          name: name,
          start_date: this.formatDate(from_time),
          end_date: this.formatDate(to_time),
          period: period,
          start_time: this.formatTime(from_time),
          end_time: this.formatTime(from_time)
      };
      this.cyclicReservations.push(reservation);
  }

  private formatDate(date: Date) {
      // transfrom month 0-11 to 1-12
      let month = date.getMonth() + 1;

      return date.getDate() + "." + this.addLeadingZero(month) + "." + date.getFullYear();
  }

  private formatTime(date: Date) {
      let hours = date.getHours();
      let minutes = date.getMinutes();

      return this.addLeadingZero(hours) + ":" + this.addLeadingZero(minutes);
  }

  private addLeadingZero(num: number) {
      let numstr: String = "";
      if (num >= 0 && num < 10) {
          numstr =  "0" + num;
      } else {
          numstr = num.toString();
      }
      return numstr;
  }
}
