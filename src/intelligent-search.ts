export class IntelligentSearchValueConverter {

  private search_string: RegExp;
  private search_value: number;

  toView(array : Array<any>, search_string: string, is_disabled: boolean) {
    if (is_disabled) {
      return array;
    } else {
      this.search_value = parseInt(search_string);
      this.search_string = new RegExp(search_string, "i");
      let to_filter = array.slice(0);
      let filtered = [];
      for (let room of to_filter) {
        if (this.is_matched(room)) {
          filtered.push(room);
        }
      }
      return filtered;
    }
  }

  private is_matched(room : any) {
    if ( (room.name.search(this.search_string) != -1)
        || (room.capacity == this.search_value)
        || (room.address.search(this.search_string) != - 1)
        || (room.description.search(this.search_string) != -1 ))
      return true;
    else
      return false;
  }
}
