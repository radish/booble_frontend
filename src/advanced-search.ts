export class AdvancedSearchValueConverter {

  searchObj: any;

  toView(array : Array<any>, searchObj: any, is_enabled: boolean) {

    if (!is_enabled) {
      return array;
    } else {
      this.searchObj = searchObj;
      console.log("AdvSearch executed");
      let to_filter = array.slice(0);
      let filtered = [];
      for (let room of to_filter) {
        if (this.is_matched(room)) {
          filtered.push(room);
        }
      }
      return filtered;
    }
  }

  private is_matched(room: any) {
    // if empty or not specified, return true
    // string exact match  => true
    // in range [min, max] => true
    let name = this.searchObj.name == ""
               || room.name.toUpperCase() == this.searchObj.name.toUpperCase();
    let min = isNaN(parseInt(this.searchObj.min))
              || (room.capacity >= this.searchObj.min);
    let max = isNaN(parseInt(this.searchObj.max))
              || (room.capacity <= this.searchObj.max);
    let address = this.searchObj.address == ""
                  || room.address.toUpperCase()
                     == this.searchObj.address.toUpperCase();
    let desc = this.searchObj.description == ""
               || room.description.toUpperCase().search(
                    this.searchObj.description.toUpperCase()) != -1;

    return (name && min && max && address && desc)
  }
}
