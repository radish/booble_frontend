export class Authenticate {
  current_form: string = 'login';

  activate(params) {
    if (params['form']) {
      this.current_form = params['form'];
    }
    else {
      this.current_form = 'login';
    }
  }
}
