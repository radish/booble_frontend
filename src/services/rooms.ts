import {HttpClient} from 'aurelia-http-client';
import {autoinject} from "aurelia-dependency-injection";

@autoinject
export default class RoomsService {
  private http: HttpClient;
  private rooms: Array<any>;

  constructor(http: HttpClient) {
    this.http = http;
  }

  async get_rooms() {
    if (this.rooms) {
      return this.rooms;
    }
    else {
      let response = await this.http.get('rooms/');
      this.rooms = response.content;

      return this.rooms;
    }
  }

  async get_room(id: number) {
    let rooms = await this.get_rooms();

    return rooms.find((room) => room.id === id);
  }
}
