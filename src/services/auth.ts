import {autoinject, Aurelia} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import appConfig from '../app-config';

@autoinject
export default class AuthService {
  urls: any = {
    login: appConfig.auth_url + 'login',
    logout: appConfig.auth_url + 'logout'
  };

  http: HttpClient;
  app: Aurelia;
  session: any = null;

  constructor(app: Aurelia, http: HttpClient) {
    this.http = http;
    this.app = app;

    this.session = JSON.parse(localStorage[appConfig.token_name] || null);
  }

  async login(email: string, password: string) {
    let response = await this.http.post(this.urls.login, {email, password});
    let auth_data = response.content;
    localStorage[appConfig.token_name] = JSON.stringify(auth_data['token']);
    this.session = auth_data['token'];
  }

  async logout() {
    try {
      await this.http.post(this.urls.logout, null);
      localStorage[appConfig.token_name] = null;
      this.session = null;
    }
    catch (error) {
      // FIXME: hey! When the token is wrong, logout fails here
      // FIXME: it's been failing for 3 months already, so yeah, fix me
      // FIXME: fast!
      console.error('Error while trying to log out: ' + JSON.stringify(error));
    }
  }

  is_authenticated(): boolean {
    return this.session != null;
  }
}
