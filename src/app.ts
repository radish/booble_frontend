import {Redirect, RouterConfiguration, Router} from 'aurelia-router';
import {autoinject} from 'aurelia-dependency-injection';
import {HttpClient, RequestMessage} from 'aurelia-http-client';
import AuthService from './services/auth';
import appConfig from './app-config';

@autoinject
export class App {
  router: Router;

  constructor(http_client: HttpClient, auth: AuthService) {
    http_client.configure(config => {
      config
        .withBaseUrl(appConfig.base_url)
        .withInterceptor({
                           request(request: RequestMessage) {
                             if (auth.is_authenticated()) {
                               request.headers.add('Authorization', 'Token ' + auth.session);
                             }
                           }
                         })
    })
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Booble';
    config.addAuthorizeStep(AuthorizeStep);
    config.map([
                 {
                   route: ['', 'home'],
                   name: 'home',
                   moduleId: './home',
                   nav: false,
                   title: 'Home',
                   auth: true
                 },
                 {
                   route: ['users', 'users/:form'],
                   name: 'users',
                   moduleId: './users',
                   nav: false,
                   title: 'Users',
                   auth: false
                 },
                 {
                   route: ['rooms', 'rooms/:roomId'],
                   name: 'rooms',
                   moduleId: './rooms',
                   nav: false,
                   title: 'Rooms',
                   auth: true
                 },
                 {
                   route: ['calendar', 'calendar/:roomId'],
                   name: 'calendar',
                   moduleId: './calendar',
                   nav: false,
                   title: 'Calendar',
                   auth: true
                 },
                 {
                   route: ['reservation', 'reservation'],
                   name: 'calendar',
                   moduleId: './reservation',
                   nav: false,
                   title: 'My Reservation',
                   auth: true
                 }
               ]);

    this.router = router;
  }
}

@autoinject
class AuthorizeStep {
  auth: AuthService;

  constructor(auth: AuthService) {
    this.auth = auth;
  }

  run(navigationInstruction, next) {
    if (navigationInstruction.getAllInstructions().some(i => i.config.auth)) {
      if (!this.auth.is_authenticated()) {
        return next.cancel(new Redirect('/users/login'));
      }
    }

    if (navigationInstruction.getAllInstructions()
        .some(i => i.config.auth === false)) {
      if (this.auth.is_authenticated()) {
        return next.cancel(new Redirect('/'));
      }
    }

    return next();
  }
}
