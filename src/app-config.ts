export default {
  base_url: 'http://localhost:8000/v0/',
  auth_url: 'users/',
  token_name: 'booble_auth',
};
